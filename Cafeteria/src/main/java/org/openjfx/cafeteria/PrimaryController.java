package org.openjfx.cafeteria;


import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventType;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import modelo.Cafe;

public class PrimaryController implements Initializable {

    @FXML
    private RadioButton btCapuccino;

    @FXML
    private RadioButton btCortado;
    
    @FXML
    private RadioButton btLatte;

    @FXML
    private Button btEliminar;

    @FXML
    private Button btPedir;

    @FXML
    private Button btRecargar;

    @FXML
    private ComboBox<String> btTamaño;

    @FXML
    private ImageView fotoCafe;

    @FXML
    private Label labelPrecio;

    @FXML
    private Label labelSaldo;

    @FXML
    private TextField textSaldo;
    
    @FXML
    private TextField tfCantidad;
    
    @FXML
    private TextField tfBuscar;

    @FXML
    private TableView<Cafe> tablaPedidos;

    @FXML
    private TableColumn colImporte;

    @FXML
    private TableColumn colTipo;

    @FXML
    private TableColumn colTamanio;

    @FXML
    private TableColumn colCantidad;

    private ObservableList<Cafe> cafes;
    private ObservableList<Cafe> cafesFiltrado;
    private ToggleGroup tg;
    double saldoTotal;
    double importeTotal;

    public void initialize(URL location, ResourceBundle resources) {

        Image image1 = new Image(getClass().getResourceAsStream("/imagen/logo2.png"));
        fotoCafe.setImage(image1);

        tg = new ToggleGroup();
        this.btCapuccino.setToggleGroup(tg);
        this.btLatte.setToggleGroup(tg);
        this.btCortado.setToggleGroup(tg);

        btTamaño.getItems().addAll("Grande", "Mediano", "Pequeño");

        cafes = FXCollections.observableArrayList();
        cafesFiltrado = FXCollections.observableArrayList();
        
        this.tablaPedidos.setItems(cafes);
        this.colTipo.setCellValueFactory(new PropertyValueFactory("tipo"));
        this.colTamanio.setCellValueFactory(new PropertyValueFactory("tamanio"));
        this.colImporte.setCellValueFactory(new PropertyValueFactory("precio"));
        this.colCantidad.setCellValueFactory(new PropertyValueFactory("cantidad"));
    }

    public void cargarSaldo(ActionEvent e) {
        
        saldoTotal = Double.parseDouble(labelSaldo.getText());
        try{
            double saldoRecarga = Double.parseDouble(textSaldo.getText());
            saldoTotal = saldoTotal + saldoRecarga;
            labelSaldo.setText(saldoTotal + "");
            textSaldo.setText("");
        }catch (NumberFormatException i) {
            Alert alert2 = new Alert(Alert.AlertType.ERROR);
            alert2.setTitle("Error");
            alert2.setContentText("Introduce una cantidad valida.");
            alert2.showAndWait();
            textSaldo.setText("");
        }
    }

    public void cobrarCafe() {
        
        importeTotal = precioCafe() * cantidadCafe();
        saldoTotal = Double.parseDouble(labelSaldo.getText());
        saldoTotal = saldoTotal - importeTotal;
        labelSaldo.setText(saldoTotal + "");
        tfCantidad.setText("");
    }

    public String tipoCafe() {
        
        RadioButton tipoCafe = (RadioButton) tg.getSelectedToggle();
        String tipo = tipoCafe.getText();
        return tipo;
    }

    public double precioCafe() {
        
        String tamanio = btTamaño.getValue();
        double precio = 0;
        if (tamanio.equalsIgnoreCase("Pequeño")) {
            precio = 0.50;
        } else if (tamanio.equalsIgnoreCase("Mediano")) {
            precio = 1;
        } else {
            precio = 1.50;
        }
        labelPrecio.setText(precio + "");
        return precio;

    }

    public int cantidadCafe() {
        
        int cantidad = Integer.parseInt(tfCantidad.getText());
        return cantidad; 
    }

    @FXML
    public void pedirCafe(ActionEvent event) {
        
        try {
            if (saldoTotal > 0 && saldoTotal >= (precioCafe()*cantidadCafe())) {
                Cafe c = new Cafe(tipoCafe(), btTamaño.getValue(), precioCafe(), cantidadCafe());
                this.cafes.add(c);
                tablaPedidos.setItems(cafes);
                cobrarCafe();
            } else {
                Alert alert1 = new Alert(Alert.AlertType.ERROR);
                alert1.setTitle("Error");
                alert1.setContentText("No tienes suficiente saldo.");
                alert1.showAndWait();
            }
        } catch (NumberFormatException e) {
            Alert alert1 = new Alert(Alert.AlertType.ERROR);
            alert1.setTitle("Error");
            alert1.setContentText("Introduce una cantidad válida.");
            alert1.showAndWait();
        } catch (NullPointerException f) {
            Alert alert1 = new Alert(Alert.AlertType.ERROR);
            alert1.setTitle("Error");
            alert1.setContentText("Selecciona un tamaño");
            alert1.showAndWait();
        }
    }
    
    @FXML
    public void eliminarCafe(ActionEvent event) {
        
        try{
            Alert alert = new Alert(AlertType.CONFIRMATION, "¿Seguro que quieres eliminar este pedido?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.isPresent() && result.get() == ButtonType.OK){
            Cafe seleccionado = tablaPedidos.getSelectionModel().getSelectedItem();
            devolverImporte(seleccionado);
            cafes.remove(seleccionado);
            }
        } catch (Exception e) {
           e.printStackTrace();
        }   
    }
    
    public void devolverImporte(Cafe seleccionado){
        
        saldoTotal = saldoTotal + seleccionado.getPrecio();
        labelSaldo.setText(saldoTotal + "");
    }
    
   @FXML
    public void filtrarCafe(KeyEvent event) {

        String filtroCafe = this.tfBuscar.getText();
        
        if (filtroCafe.isEmpty()){
            this.tablaPedidos.setItems(cafes);
        }else {
            this.cafesFiltrado.clear();
            for (Cafe c:this.cafes) {
                if (c.getTipo().toLowerCase().contains(filtroCafe.toLowerCase())){
                    this.cafesFiltrado.add(c);
                }
            }
            this.tablaPedidos.setItems(cafesFiltrado);
        }
    }
}
