package org.openjfx.bebidasapi;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {
        String css = getClass().getResource("/Estilos/estilos.css").toExternalForm();
        
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("primary.fxml"));
        //Rectangle2D screenBounds = Screen.getPrimary().getBounds(); // Para pillar las dimensiones de la pantalla
        Scene scene = new Scene(fxmlLoader.load()/*, (screenBounds.getWidth()), (screenBounds.getHeight()-100)*/);
        scene.getStylesheets().add(css);
        stage.setScene(scene);
        stage.show();
    }


    public static void main(String[] args) {
        launch();
    }

}