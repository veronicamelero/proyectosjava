
package proyectojuego;

public class Combate {
    
    int numCombate = 0;
    String ganador;
    int bolas;
    public String personajes;

    public Combate(int numCombate ,String personajesPelea, String ganador, int bolas) {
        this.personajes = personajesPelea;
        this.ganador = ganador;
        this.numCombate = numCombate;
        this.bolas = bolas;
    }

    public int getNumCombate() {
        return numCombate;
    }

    public void setNumCombate(int numCombate) {
        this.numCombate ++;
    }

    public String getGanador() {
        return ganador;
    }

    public void setGanador(String ganador) {
        this.ganador = ganador;
    }

    public int getBolas() {
        return bolas;
    }

    public void setBolas(int bolas) {
        this.bolas = bolas;
    }

    public String getPersonajes() {
        return personajes;
    }

    public void setPersonajes(String personajes) {
        this.personajes = personajes;
    }


    
  
    
    
    
    
}
