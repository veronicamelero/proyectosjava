
package modelo;

public class Cafe {
    
   String tipo;
   String tamanio;
   Double precio;
   int cantidad;

    public Cafe(String tipo, String tamanio, Double precio, int cantidad) {
        this.tipo = tipo;
        this.tamanio = tamanio;
        this.precio = precio * cantidad;
        this.cantidad = cantidad;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTamanio() {
        return tamanio;
    }

    public void setTamanio(String tamanio) {
        this.tamanio = tamanio;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public String toString() {
        return "Cafe{" + "tipo=" + tipo + ", tamanio=" + tamanio + ", precio=" + precio + '}';
    }
   
   
    
}
