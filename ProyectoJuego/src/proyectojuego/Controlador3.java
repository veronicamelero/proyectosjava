
package proyectojuego;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import static proyectojuego.Controlador2.escenarioElegido;
import static proyectojuego.Controlador2.personajesCombate;



public class Controlador3 implements Initializable {
    
    private Scene scene;
    private Stage stage;
    
    private MediaPlayer musicaCombate;
    private MediaPlayer efectoCombate;
    
    //IMAGENES
    @FXML
    private ImageView imgFondo;
    @FXML
    private ImageView imgPersonaje1;
    @FXML
    private ImageView imgPersonaje2;
    @FXML
    private ImageView imgAtras;
    @FXML
    private ImageView mute3;
    @FXML
    private ImageView btSonido;
    
    @FXML
    private ImageView imgKame;
    @FXML
    private ImageView imgKame2;
    
    //BOTONES
    @FXML
    private Button btAtacarI;
    @FXML
    private Button btAtras;
    @FXML
    private Button btVerTabla;
    @FXML
    private Button btRevancha;
    @FXML
    private Button btEspecial1;
    
    Personaje personaje1, personaje2;
    
    //BARRA
    @FXML
    private ProgressBar barraD;
    @FXML
    private ProgressBar barraI;
    @FXML
    private ProgressBar barraE1;
    @FXML
    private ProgressBar barraE2;    
    
    //LABEL
    @FXML
    private Label lbInformacion;
     @FXML
    private Label lbP1;
    @FXML
    private Label lbP2;
     
     
    double daño = 1;
    double estaminaI = 0;
    double estaminaD = 0;
    public static int numCombate;
    public static ArrayList <Combate> combates = new ArrayList <>();
    public Combate c;
    public static int bolasJ1=6;
    public static int bolasJ2;
    public int ganador;
    public static boolean silencio = false;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        if (silencio == false){
            sonido("src/Musica/FinalDestination.mp3", true); 
        }
        Image escenario = new Image(getClass().getResourceAsStream(escenarioElegido));
        imgFondo.setImage(escenario);
        imgKame.setVisible(false);
        imgKame2.setVisible(false);
        
        
        personaje1 = personajesCombate.get(0);
        personaje2 = personajesCombate.get(1);

        cargarPersonajeIzquierda();
        cargarPersonajeDerecha();
        
        lbP1.setText("J1 - "+personaje1.getNombre());
        lbP2.setText("J2 - "+personaje2.getNombre());
        
        btAtras.setVisible(false);
        btVerTabla.setVisible(false);
        btRevancha.setVisible(false);
        btEspecial1.setDisable(true);
        
        empiezaPelea();
        
        estilosBarra();
    } 
    
    public void sonido(String url, boolean bucle) {
        Media media = new Media(new File(url).toURI().toString());
        if (bucle) {
            musicaCombate = new MediaPlayer(media);
            musicaCombate.setCycleCount(MediaPlayer.INDEFINITE);
            musicaCombate.setVolume(0.6);
            musicaCombate = new MediaPlayer(media);
            musicaCombate.play();
        } else {
            efectoCombate = new MediaPlayer(media);
            efectoCombate.setCycleCount(1);
            efectoCombate = new MediaPlayer(media);
            efectoCombate.play();
        }
    }
    
    @FXML
    void quitarSonido(MouseEvent event) {
        musicaCombate.setMute(true);
        efectoCombate.setMute(true);
        silencio = true;
    }
    
    @FXML
    void ponerSonido(MouseEvent event) {
        silencio = false;
        musicaCombate.setMute(false);
        efectoCombate.setMute(false);
    }
    
    private void cargarPersonajeIzquierda() {
        Image pers1 = new Image(getClass().getResourceAsStream(personaje1.getGif1()));
        imgPersonaje1.setImage(pers1);
    }

    private void cargarPersonajeDerecha() {
        Image pers2 = new Image(getClass().getResourceAsStream(personaje2.getGif1()));
        imgPersonaje2.setImage(pers2);   
    }
    
    public static void delay(long millis, Runnable continuation) {
        Task<Void> sleeper = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                try {
                    Thread.sleep(millis);
                } catch (InterruptedException e) {
                }
                return null;
            }
        };
        sleeper.setOnSucceeded(event -> continuation.run());
        new Thread(sleeper).start();
    }
    
    public void moverImagen(int posX, long duracion, ImageView image) {
        TranslateTransition translate = new TranslateTransition();
        translate.setNode(image);
        translate.setDuration(Duration.millis(duracion));
        translate.setByX(posX);
        translate.play();
    }
    
    private void vaciarLabel() {
        delay(5000, () -> lbInformacion.setText(""));
    }
    
    // METODOS BOTONES COMBATE
    
    private void mostrarBotones() {
        btAtras.setVisible(true);
        btVerTabla.setVisible(true);
        btRevancha.setVisible(true);
    }
    
    public void desactivarBotonesAtaque() {
        btAtacarI.setDisable(true);
        btEspecial1.setDisable(true);
    }
    
    void activarBotonesAtaque(){
        btAtacarI.setDisable(false);
        if (barraE1.getProgress() >= 1) {
            btEspecial1.setDisable(false);
        }
    }
    
    // METODOS ANIMACIONES
    
    void animacionAtaqueNormal(String lado) {
        if (lado.equals("izquierdo")) {
            Image stand1 = new Image(personaje1.getGif2());
            moverImagen((500), 700, imgPersonaje1);
            if (silencio == false){
                delay(900, () -> sonido("src/Musica/hit.mp3", false));
            }
            imgPersonaje1.setImage(stand1);
            delay(1000, () -> cargarPersonajeIzquierda());
            delay(1500, () -> moverImagen((-500), 700, imgPersonaje1));    
            Image dolor = new Image (personaje2.especial[1]);
            delay(700, () -> imgPersonaje2.setImage(dolor));
            delay(1000, () -> cargarPersonajeDerecha());    
        } else {
            Image stand = new Image(personaje2.getGif2());
            moverImagen((-500), 700, imgPersonaje2);
            if (silencio == false){
                delay(900, () -> sonido("src/Musica/hit.mp3", false));
            }
            imgPersonaje2.setImage(stand);
            delay(1000, () -> cargarPersonajeDerecha());
            delay(1000, () -> moverImagen((500), 700, imgPersonaje2));
            Image dolor = new Image (personaje1.especial[1]);
            delay(700, () -> imgPersonaje1.setImage(dolor));
            delay(1000, () -> cargarPersonajeIzquierda()); 
        }
    }
    
    void animacionAtaqueEspecial(String lado){
        if (lado.equals("izquierdo")) {
            Image transformacion = new Image(personaje1.especial[0]);
            Image normal = new Image(personaje1.getGif1());
            if (personaje1.getNombre().equals("Goku") || personaje1.getNombre().equals("Vegeta") || personaje1.getNombre().equals("Cell")) {
                Image dolor = new Image (personaje2.especial[1]);
                if (silencio == false){
                delay(900, () -> sonido("src/Musica/kame.mp3", false));
                }
                imgPersonaje1.setImage(transformacion);
                delay(1000, () -> imgPersonaje2.setImage(dolor));
                delay(1400, () -> cargarPersonajeDerecha());
                delay(700, () -> imgKame.setVisible(true));
                delay(1500, () -> imgKame.setVisible(false));
            } else {
                moverImagen((500), 700, imgPersonaje1);
                if (silencio == false){
                    delay(900, () -> sonido("src/Musica/hit.mp3", false));
                }
                imgPersonaje1.setImage(transformacion);
                delay(1500, () -> imgPersonaje1.setImage(normal));
                Image dolor = new Image (personaje2.especial[1]);
                delay(700, () -> imgPersonaje2.setImage(dolor));
                delay(1000, () -> cargarPersonajeDerecha()); 
                delay(1500, () -> moverImagen((-500), 700, imgPersonaje1)); 
            }
        } else {
            Image transformacion = new Image(personaje2.especial[0]);
            Image normal = new Image(personaje2.getGif1());
            
            imgPersonaje2.setImage(transformacion);
            delay(1500, () -> imgPersonaje2.setImage(normal)); 
            if (personaje2.getNombre().equals("Goku") || personaje2.getNombre().equals("Vegeta") || personaje2.getNombre().equals("Cell")) {
                Image dolor = new Image (personaje1.especial[1]);
                if (silencio == false){
                    delay(900, () -> sonido("src/Musica/kame.mp3", false));
                }
                delay(1000, () -> imgPersonaje1.setImage(dolor));
                delay(1400, () -> cargarPersonajeIzquierda());
                delay(700, () -> imgKame2.setVisible(true));
                delay(1500, () -> imgKame2.setVisible(false));
            } else {
                moverImagen((-500), 700, imgPersonaje2);
                if (silencio == false){
                    delay(900, () -> sonido("src/Musica/hit.mp3", false));
                }
                Image dolor = new Image (personaje1.especial[1]);
                delay(700, () -> imgPersonaje1.setImage(dolor));
                delay(1000, () -> cargarPersonajeIzquierda());
                delay(1000, () -> moverImagen((500), 700, imgPersonaje2));
            }
        }
    }
    
    void animacionMuerte(String lado){
        if (lado.equals("izquierdo")) {
            Image muerte = new Image(personaje1.especial[2]);
            delay(1000, () -> imgPersonaje1.setImage(muerte));
            imgPersonaje1.setImage(muerte);
            
        } else {
            Image muerte = new Image(personaje2.especial[2]);
            delay(1000, () -> imgPersonaje2.setImage(muerte));
            imgPersonaje2.setImage(muerte);
        }
    }
    
    void animacionGanar(String lado){
        if (lado.equals("izquierdo")) {
            Image gana = new Image(personaje1.especial[3]);
            delay(1000, () -> imgPersonaje1.setImage(gana));
            imgPersonaje1.setImage(gana);
            
        } else {
            Image gana = new Image(personaje2.especial[3]);
            delay(1000, () -> imgPersonaje2.setImage(gana));
            imgPersonaje2.setImage(gana);
        }
    }
    
    
// METODOS PROGRESS BAR
    
    private void estilosBarra() {
        if (barraD.getProgress() < 0.5 && barraD.getProgress() > 0.2) {
            barraD.setStyle("-fx-accent: #EC7315;");
        } else if (barraD.getProgress() <= 0.2) {
            barraD.setStyle("-fx-accent: #F5320F;");
        } else {
            barraD.setStyle("-fx-accent: #14C624;"); 
        }
        
        if (barraI.getProgress() < 0.5 && barraI.getProgress() > 0.2) {
            barraI.setStyle("-fx-accent: #EC7315;");
        } else if (barraI.getProgress() <= 0.2) {
            barraI.setStyle("-fx-accent: #F5320F;");
        } else {
            barraI.setStyle("-fx-accent: #14C624;"); 
        }
    }
    
    public void estilosBarraEstamina() {
        if (barraE1.getProgress() >= 1) {
            barraE1.setStyle("-fx-accent: #684FD5;");
        } else {
            barraE1.setStyle("-fx-accent: yellow;");
        }
        
        if (barraE2.getProgress() >= 1) {
            barraE2.setStyle("-fx-accent: #684FD5;");
        } else {
            barraE2.setStyle("-fx-accent: yellow;");
        } 
    }

// METODOS ATAQUE 
    
    //Metodo que mediante un aleatorio decide que jugador empieza el combate
    private void empiezaPelea() {
        boolean empieza;
        Double aleatorio = (Math.random()*10+1);
        
        if (aleatorio <= 5){
            empieza = true;
        } else {
            empieza = false;
        }
        
        if (empieza == true){
            lbInformacion.setText("Empieza el Jugador 1 con "+personaje1.getNombre());
            btAtacarI.setDisable(false);
            vaciarLabel();
        } else {
            lbInformacion.setText("Empieza el Jugador 2 con "+personaje2.getNombre());
            btAtacarI.setDisable(true);
            atacarDcha();
            vaciarLabel();
        }
    }
    
    private boolean comprobarVidaIzquierda() {
        boolean vida = true;
        if (barraI.getProgress() <= 0.0) {
            vida = false;
        }
        return vida;
    }
    
    private boolean comprobarVidaDerecha() {
        boolean vida = true;
        if (barraD.getProgress() <= 0.0) {
            vida = false;
        }
        return vida;
    }
     
    @FXML
    void atacarIzda(ActionEvent event) {
        desactivarBotonesAtaque();
        personaje1 = personajesCombate.get(0);
        animacionAtaqueNormal("izquierdo");
        daño -= personaje1.atacar() * 0.01;
        barraD.setProgress(daño);
        if (personaje1.aCritico == true) {
            lbInformacion.setText("Ataque crítico!!!");
            vaciarLabel();
        }
        estaminaI = estaminaI + 0.2;
        barraE1.setProgress(estaminaI);
        estilosBarraEstamina();
        if (comprobarVidaDerecha()) {
            estilosBarra();
            if (barraE2.getProgress() < 1) {
                delay(3000, () -> atacarDcha());
            } else if (barraE2.getProgress() >= 1) {
                delay(3000, () -> ataqueEspecialD());  
            }
        } else {
            animacionMuerte("derecho");
            animacionGanar("izquierdo");
            finCombate("izquierdo");
        }
    }
    
    void atacarDcha() {
        desactivarBotonesAtaque();
        personaje2 = personajesCombate.get(1);
        animacionAtaqueNormal("derecho");
        daño -= personaje2.atacar() * 0.01;
        barraI.setProgress(daño);
        if (personaje2.aCritico == true) {
            lbInformacion.setText("Ataque crítico!!!");
            vaciarLabel();
        }
        estaminaD = estaminaD + 0.2;
        barraE2.setProgress(estaminaD);
        estilosBarraEstamina();
        if (comprobarVidaIzquierda()) {
            estilosBarra();
            delay(2000, () -> activarBotonesAtaque());
        } else {
            animacionMuerte("izquierdo");
            animacionGanar("derecho");
            finCombate("derecho");
        }  
    }
    
    void ataqueEspecialD() {
        estilosBarraEstamina();
        desactivarBotonesAtaque();
        barraE2.setProgress(0.0);
        estaminaD = 0;
        animacionAtaqueEspecial("derecho");
        daño -= (personaje2.atacar() + 10) * 0.01;
        barraI.setProgress(daño);
        if (comprobarVidaIzquierda()) {
            estilosBarra();
            delay(2000, () -> activarBotonesAtaque());
        } else {
            delay(3000, () -> animacionMuerte("izquierdo"));
            delay(3000, () -> animacionGanar("derecho"));
            finCombate("derecho");
        }
    }
 
    @FXML
    void ataqueEspecialI(ActionEvent event) {
        desactivarBotonesAtaque();
        estilosBarraEstamina();
        barraE1.setProgress(0.0);
        estaminaI = 0;
        animacionAtaqueEspecial("izquierdo");
        daño -= (personaje1.atacar() + 10) * 0.01;
        barraD.setProgress(daño);
        if (comprobarVidaDerecha()) {
            estilosBarra();
            if (barraE2.getProgress() < 1) {
                delay(3000, () -> atacarDcha());
            } else if (barraE2.getProgress() >= 1) {
                delay(3000, () -> ataqueEspecialD());
            }
        } else {
            delay(3000, () -> animacionMuerte("derecho"));
            delay(3000, () -> animacionGanar("izquierdo"));
            finCombate("izquierdo");
        }
    }

    void finCombate(String lado) {
        if (lado.equals("izquierdo")) {
            if (silencio == false) {
                musicaCombate.stop();
                sonido("src/Musica/victory.mp3", true);
            }
            lbInformacion.setText("FIN DEL COMBATE");
            vaciarLabel();
            lbInformacion.setText("Ha ganado Jugador 1 con " + personaje1.getNombre());
            ganador = 1;
            desactivarBotonesAtaque();
            datosCombate(1);
            if (bolasJ1 == 7) {
                delay(4000, () -> musicaCombate.stop());
                delay(6000, () -> comprobarBolas());
            }
            mostrarBotones();
        } else {
            if (silencio == false) {
                musicaCombate.stop();
                sonido("src/Musica/sadness.mp3", true);
            }
            lbInformacion.setText("Fin del combate");
            vaciarLabel();
            lbInformacion.setText("Ha ganado Jugador 2 con " + personaje2.getNombre());
            ganador = 2;
            desactivarBotonesAtaque();
            datosCombate(2);
            mostrarBotones();
        }
    }

    // En funcion del ganador crea un objeto combate con ese ganador para enviarlo al observable list e incrementa las bolas de dragon (los combates ganados)
    private void datosCombate(int ganador) {
        String jugadores = "J1 - " + personaje1.getNombre() + " VS J2 - " + personaje2.getNombre();
        numCombate++;
        if (ganador == 1) {
            bolasJ1++;
            c = new Combate(numCombate, jugadores, "Jugador 1", bolasJ1);
            combates.add(c);
        } else if (ganador == 2) {
            bolasJ2++;
            c = new Combate(numCombate, jugadores, "Jugador 2", bolasJ2);
            combates.add(c);
        }
    }

    // Si se han conseguido las 7 bolas de dragon carga una ventana nueva
    void comprobarBolas() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("FXMLBolas.fxml"));
            Parent root = loader.load();
            
            ControladorBolas controlador = loader.getController();

            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(scene);
            stage.showAndWait();

            bolasJ1 = 0;
        } catch (IOException ex) {
            Logger.getLogger(Controlador3.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     
    // IMAGEN VOLVER ATRAS. Vuelve a la ventana de elegir personajes en cualquier momento
    @FXML
    void volverAtras(MouseEvent event) {
        musicaCombate.stop();
        try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("FXML2.fxml"));
                Parent root = loader.load();
                Controlador2 controlador2 = loader.getController();

                stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
                
                personajesCombate.clear();

            } catch (IOException io) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText(null);
                alert.setTitle("Error");
                alert.setContentText(io.getMessage());
                alert.showAndWait();
            }
    }

    // BOTON DE NUEVO COMBATE. Vuelve a la ventana de elegir personajes
    @FXML
    void volverAtrasBoton(ActionEvent event) {
        musicaCombate.stop();
          try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("FXML2.fxml"));
                Parent root = loader.load();
                Controlador2 controlador2 = loader.getController();

                stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
                
                personajesCombate.clear();

            } catch (IOException io) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText(null);
                alert.setTitle("Error");
                alert.setContentText(io.getMessage());
                alert.showAndWait();
            }
    } 
    
    //BOTON  REVANCHA
    @FXML
    void revancha(ActionEvent event) {
        musicaCombate.stop();
        //sonido("src/Musica/FinalDestination.mp3", true);
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("FXML3.fxml"));
                Parent root;
            root = loader.load();
             Controlador3 controlador3 = loader.getController();
                //controlador3.initAtributtes(personajesCombate, escenarioElegido);

                stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                scene = new Scene(root);
                stage.setScene(scene);
                stage.show();  
        } catch (IOException ex) {
            Logger.getLogger(Controlador3.class.getName()).log(Level.SEVERE, null, ex);
        }
               
    }
    
    //BOTON VER TABLA
    @FXML
    void verTabla(ActionEvent event) {
        try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("FXMLTabla.fxml"));
                Parent root = loader.load();

                ControladorTabla controlador = loader.getController();

                Scene scene = new Scene(root);
                Stage stage = new Stage();
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.setScene(scene);
                stage.showAndWait();
                
            } catch (IOException e) {   
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText(null);
                alert.setTitle("Error");
                alert.setContentText(e.getMessage());
                alert.showAndWait();
            }
       }
}
