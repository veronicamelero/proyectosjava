
package proyectojuego;

public class Personaje {

    String nombre;
    int vida;
    String img1;
    String gif1, gif2;
    String img3;
    Boolean aCritico;
    String [] especial;

    public Personaje(String nombre, int vida, String img1, String gif1, String img3, String gif2, Boolean critico, String [] especial) {
        this.nombre = nombre;
        this.vida = vida;
        this.img1 = img1;
        this.gif1 = gif1;
        this.gif2 = gif2;
        this.img3 = img3;
        this.aCritico = critico;
        this.especial = especial;    
    }

    public String getGif1() {
        return gif1;
    }

    public void setGif1(String gif1) {
        this.gif1 = gif1;
    }
    
    public String getGif2() {
        return gif2;
    }

    public void setGif2(String gif1) {
        this.gif2 = gif2;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getVida() {
        return vida;
    }

    public void setVida(int vida) {
        this.vida = vida;
    }

    public String getImg1() {
        return img1;
    }

    public void setImg1(String img1) {
        this.img1 = img1;
    }


    public String getImg3() {
        return img3;
    }

    public void setImg3(String img3) {
        this.img3 = img3;
    }

    @Override
    public String toString() {
        return "Personaje: /n" + "Nombre: " + nombre + "/n Vida: " + vida;
    }

    public double atacar() {
        aCritico = false;
        double critico = Math.random() * 5 + 1;
        double daño = (Math.random() * 10 + 1);
        if (critico >= 3 && critico <= 3.5) {
            daño = daño + 5;
            aCritico = true;
        }
        return daño;
    }
}
