
package proyectojuego;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.effect.BoxBlur;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import static proyectojuego.Controlador3.silencio;


public class Controlador2 implements Initializable {

    protected Scene scene;
    protected Stage stage;
    
    private MediaPlayer musicaElegir;
    
    //BOTONES
    @FXML
    private Button btPelea;
    @FXML
    private ImageView mute2;
    @FXML
    private ImageView btSonido;
   
    // IMAGENES
    @FXML
    private ImageView Bu;
    @FXML
    private ImageView N18;
    @FXML
    private ImageView Freezer;
    @FXML
    private ImageView Goku;
    @FXML
    private ImageView Cell;
    @FXML
    private ImageView Vegeta;
    @FXML
    private ImageView fondoPersonajes;
    @FXML
    private ImageView imgPersonajeDerecha;
    @FXML
    private ImageView imgPersonajeIzquierda;
    @FXML
    private ImageView imgKame;
    @FXML
    private ImageView imgNamek;
    @FXML
    private ImageView imgTorneo;
    @FXML
    public Image gokuG, vegetaG, n18G, buG, freezerG, cellG; 
    
    @FXML
    BoxBlur boxblur;
    
    public static String [] especialGoku = {"/Imagenes/especialGoku/especial1.gif", "/Imagenes/gokuDolor.gif", "/Imagenes/GokuDerrota.gif", "/Imagenes/GokuVictoria.gif"};
    public static String [] especialVegeta = {"Imagenes/especialVegeta/especial1.gif", "/Imagenes/vegetaDolor.gif", "/Imagenes/VegetaDerrota.gif", "/Imagenes/VegetaVictoria.gif"};
    public static String [] especialCell = {"Imagenes/especialCell/especial2.gif", "/Imagenes/cellDolor.gif", "/Imagenes/CellDerrota.gif", "/Imagenes/CellVictoria.gif"};
    public static String [] especialBu = {"Imagenes/especialBu/especial3.gif", "/Imagenes/BuuDolor.gif", "/Imagenes/BuuDerrota.gif", "/Imagenes/BuuVictoria.gif"};
    public static String [] especialFreezer = {"Imagenes/especialFreezer/especial1.gif", "/Imagenes/freezerDolor.gif", "/Imagenes/FreezerDerrota.gif", "/Imagenes/FreezerVictoria.gif"};
    public static String [] especialN18 = {"Imagenes/especialN18/especial2.gif", "/Imagenes/N18Dolor.gif", "/Imagenes/N18Derrota.gif", "/Imagenes/N18Victoria.gif"};
    
    public static Personaje pGoku = new Personaje("Goku", 100, "/Imagenes/GokuCara.png", "/Imagenes/GokuPose.gif", "/Imagenes/GokuGrande.png","/Imagenes/GokuGolpe.gif", false, especialGoku);
    public static Personaje pVegeta = new Personaje("Vegeta", 100, "/Imagenes/VegetaCara.png", "/Imagenes/VegetaPose.gif", "/Imagenes/VegetaGrande.png","/Imagenes/VegetaGolpe.gif", false, especialVegeta);
    public static Personaje pCell = new Personaje("Cell", 100, "/Imagenes/CellCara.png", "/Imagenes/CellPose.gif", "/Imagenes/CellGrande.png","/Imagenes/CellGolpe.gif", false, especialCell);
    public static Personaje pBu = new Personaje("Bu", 100, "/Imagenes/BuCara.png", "/Imagenes/BuuPose.gif", "/Imagenes/BuGrande.png","/Imagenes/BuuGolpe.gif", false, especialBu);
    public static Personaje pFreezer = new Personaje("Freezer", 100, "/Imagenes/FreezerCara.png", "/Imagenes/FreezerPose.gif", "/Imagenes/FreezerGrande.png","/Imagenes/FreezerGolpe.gif", false, especialFreezer);
    public static Personaje pN18 = new Personaje("Androide 18", 100, "/Imagenes/N18Cara.png", "/Imagenes/N18pose1.gif", "/Imagenes/N18.png","/Imagenes/N18Ataque.gif", false, especialN18);
            
    public static ArrayList <Personaje> personajesCombate = new ArrayList <>();
    
    private boolean ladoIzquierdo = true;
    public static String escenarioElegido = "";
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        crearImagenes();
        btPelea.setDisable(true);
        if (silencio == false){
            sonido("src/Musica/ChalaHeadChala.mp3");
        }
    }
    
    public void sonido(String url) {
        Media media = new Media(new File(url).toURI().toString());
            musicaElegir = new MediaPlayer(media);
            musicaElegir.setCycleCount(MediaPlayer.INDEFINITE);
            musicaElegir.setVolume(0.6);
            musicaElegir = new MediaPlayer(media);
            musicaElegir.play();
    }
    
    @FXML
    void quitarSonido(MouseEvent event) {
        musicaElegir.setMute(true);
        silencio = true;
    }
    
    @FXML
    void ponerSonido(MouseEvent event) {
        silencio = false;
        musicaElegir.setMute(false);
    }
    
    private void crearImagenes() {
        gokuG = new Image(getClass().getResourceAsStream(pGoku.getImg3()));
        vegetaG = new Image(getClass().getResourceAsStream(pVegeta.getImg3()));
        cellG = new Image(getClass().getResourceAsStream(pCell.getImg3()));
        buG = new Image(getClass().getResourceAsStream(pBu.getImg3()));
        freezerG = new Image(getClass().getResourceAsStream(pFreezer.getImg3()));
        n18G = new Image(getClass().getResourceAsStream(pN18.getImg3()));
    }
     
    @FXML
    void mostrarPersonajes(MouseEvent event) {
        if (ladoIzquierdo == true){
            personaje (event, imgPersonajeIzquierda);
        } else {
            personaje (event, imgPersonajeDerecha);
        }
    }
    
    private void personaje(MouseEvent event, ImageView image) {
        if (event.getSource().equals(Goku)) {    
            image.setImage(gokuG);
        } else if (event.getSource().equals(Vegeta)) {        
            image.setImage(vegetaG);
        } else if (event.getSource().equals(Cell)) {            
            image.setImage(cellG);
        } else if (event.getSource().equals(Bu)) {            
            image.setImage(buG);
        } else if (event.getSource().equals(Freezer)) {            
            image.setImage(freezerG);
        } else if (event.getSource().equals(N18)) {            
            image.setImage(n18G);
        }
    }
    
    @FXML
    void personajeClick(MouseEvent event) {
        if (ladoIzquierdo == true) {
            elegirPersonaje(imgPersonajeIzquierda);
            ladoIzquierdo = false;
        } else {           
            elegirPersonaje (imgPersonajeDerecha);
            desactivarImagenes();
            btPelea.setDisable(false);
        }
    }
 
    public void desactivarImagenes() {
        Goku.setDisable(true);
        Bu.setDisable(true);
        Vegeta.setDisable(true);
        N18.setDisable(true);
        Freezer.setDisable(true);
        Cell.setDisable(true);
    }
    
    private void elegirPersonaje(ImageView image) {
        if (image.getImage().equals(gokuG)){
            personajesCombate.add(pGoku);
        } else if (image.getImage().equals(vegetaG)) {
            personajesCombate.add(pVegeta);
        } else if (image.getImage().equals(cellG)) {
            personajesCombate.add(pCell);
        } else if (image.getImage().equals(buG)){
            personajesCombate.add(pBu);
        } else if (image.getImage().equals(freezerG)){
            personajesCombate.add(pFreezer);
        } else if (image.getImage().equals(n18G)){
            personajesCombate.add(pN18);
        }
    }
    
    @FXML
    public void elegirEscenario(MouseEvent event) {
        boxblur = new BoxBlur();   
        if (event.getSource().equals(imgKame)) {
            imgKame.setEffect(boxblur);
            escenarioElegido = ("/Imagenes/kame1.jpg");
            imgTorneo.setDisable(true);
            imgNamek.setDisable(true);
        } else if (event.getSource().equals(imgTorneo)){
            escenarioElegido = "/Imagenes/torneo1.jpg";
            imgTorneo.setEffect(boxblur);
            imgKame.setDisable(true);
            imgNamek.setDisable(true);
        } else if (event.getSource().equals(imgNamek)){
            escenarioElegido = "/Imagenes/namek1.jpg";
            imgNamek.setEffect(boxblur);
            imgKame.setDisable(true);
            imgTorneo.setDisable(true);
        } else {
            escenarioElegido = "";
        }
    }
    
    @FXML
    void iniciarPelea(ActionEvent event) {
        if (!escenarioElegido.equals("")) {
            try {
                musicaElegir.stop();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("FXML3.fxml"));
                Parent root = loader.load();
                Controlador3 controlador3 = loader.getController();
                //controlador3.initAtributtes(personajesCombate, escenarioElegido);

                stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                scene = new Scene(root);
                stage.setScene(scene);
                stage.show();

            } catch (IOException io) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText(null);
                alert.setTitle("Error");
                alert.setContentText(io.getMessage());
                alert.showAndWait();
            }
        } else {
            Alert alert1 = new Alert(Alert.AlertType.ERROR);
            alert1.setTitle("Error");
            alert1.setContentText("Elige un escenario para el combate");
            alert1.showAndWait();
        }
    }
}

