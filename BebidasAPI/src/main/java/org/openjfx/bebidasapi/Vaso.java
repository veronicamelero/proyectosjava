
package org.openjfx.bebidasapi;

public class Vaso {
   
    String nombre;

    public Vaso(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    @Override
    public String toString() {
        return getNombre();
    }
}
