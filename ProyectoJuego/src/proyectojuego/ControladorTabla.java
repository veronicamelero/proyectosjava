
package proyectojuego;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import static proyectojuego.Controlador3.combates;


public class ControladorTabla implements Initializable {
    
    private Scene scene;
    private Stage stage;

    //IMAGENES
    @FXML
    private ImageView imgFondoTabla;
    @FXML
    private ImageView logo;

    //TABLA
    @FXML
    private TableView <Combate> tablaCombates;
      @FXML
    private TableColumn colBolas;
    @FXML
    private TableColumn colPersonajeGanador;
    @FXML
    private TableColumn colPersonajes;
     @FXML
    private TableColumn colNumCombate;
     
    private ObservableList <Combate> listaCombates;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {    
        //listaCombates.addAll(combates);
        this.colNumCombate.setCellValueFactory(new PropertyValueFactory("NumCombate"));
        this.colPersonajes.setCellValueFactory(new PropertyValueFactory("Personajes"));
        this.colPersonajeGanador.setCellValueFactory(new PropertyValueFactory("Ganador"));
        this.colBolas.setCellValueFactory(new PropertyValueFactory("Bolas"));
        listaCombates = FXCollections.observableArrayList();
        listaCombates.addAll(combates);
        this.tablaCombates.setItems(listaCombates); 

    }


}
