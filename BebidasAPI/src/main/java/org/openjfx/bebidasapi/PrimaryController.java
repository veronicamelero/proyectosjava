package org.openjfx.bebidasapi;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Border;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import org.json.JSONArray;
import org.json.JSONObject;

public class PrimaryController implements Initializable{

    // BOTONES
    @FXML
    private Button btCats;
    @FXML
    private Button btRandom;
    @FXML
    private Button btVaso;
    @FXML
    private Button btIngredientes;
    @FXML
    private Button btNombre;
    
    // RADIOBUTTON
    @FXML
    private RadioButton btCon;
    @FXML
    private RadioButton btSin;
    
    // TOGGLEGROUP
    private ToggleGroup tg;

    // LABEL
    @FXML
    private Label lbInfo;
    
    // TEXTFIELD
    @FXML
    private TextField tfNombre;
    
    // IMAGEN
    @FXML
    private ImageView imgBebida;

    // TABLA
    @FXML
    private TableView<Bebida> tbBebidas;
    @FXML
    private TableColumn colId;
    @FXML
    private TableColumn colNombre;
    
    // OBSERVABLE LIST
    private ObservableList<Categoria> listaCategorias;
    private ObservableList<Vaso> listaVasos;
    private ObservableList<Bebida> listaBebidas;
    private ObservableList<Ingrediente> listaIngredientes;
    
    // CHOICE BOX
    @FXML
    private ChoiceBox<Categoria> cbCategorias;
    @FXML
    private ChoiceBox<Vaso> cbVasos;
    @FXML
    private ChoiceBox<Ingrediente> cbIngredientes;
    
    
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        instancias();
        try {
            jsonCategorias();
            jsonVasos();
            jsonIngredientes();
        } catch (IOException ex) {
            Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
        }
        asociarElementos();
    }
    

    private void instancias() {
        listaCategorias = FXCollections.observableArrayList();
        listaVasos = FXCollections.observableArrayList();
        listaBebidas = FXCollections.observableArrayList();
        listaIngredientes = FXCollections.observableArrayList();
        tg = new ToggleGroup();
        this.btCon.setToggleGroup(tg);
        this.btSin.setToggleGroup(tg);
    }
    
    private void asociarElementos() {
        cbCategorias.setItems(listaCategorias);
        cbVasos.setItems(listaVasos);
        cbIngredientes.setItems(listaIngredientes);
    }
    
    // Método que recibe un String con la url de la API y devuelve un String con todo el contenido de la página
    private String extraerFichero(String url) throws IOException{
        String fichero = "";
        InputStream inputStream;
        inputStream = new URL(url).openStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String linea;
            while((linea = bufferedReader.readLine()) != null) {
                fichero += linea;
            }
        return fichero;
    }
    
    // Método para extraer las categorías y añadirlas al Observable List de categorías
    private void jsonCategorias() throws MalformedURLException, IOException { 
        String fichero = extraerFichero("https://www.thecocktaildb.com/api/json/v1/1/list.php?c=list");    
        JSONObject obj = new JSONObject(fichero); 
        JSONArray drinks = obj.getJSONArray("drinks");
        int n = drinks.length();
        for (int i = 0; i < n; ++i) { 
            JSONObject cats = drinks.getJSONObject(i);
            String nombreCategoria = cats.getString("strCategory");
            listaCategorias.add(new Categoria(nombreCategoria));
        }    
    }
    
    // Método para extraer los tipos de vaso y añadirlos al Observable List de vasos
    private void jsonVasos() throws IOException {
        String fichero = extraerFichero("https://www.thecocktaildb.com/api/json/v1/1/list.php?g=list");    
        JSONObject obj = new JSONObject(fichero);
        JSONArray drinks = obj.getJSONArray("drinks");
        int n = drinks.length();
        for (int i = 0; i < n; ++i) { 
            JSONObject v = drinks.getJSONObject(i);
            String nombreVaso = v.getString("strGlass");
            listaVasos.add(new Vaso(nombreVaso));
        }    
    }
    
    // Método para extraer los ingredientes y añadirlos al Observable List de ingredientes
    private void jsonIngredientes() throws IOException {
        String fichero = extraerFichero("https://www.thecocktaildb.com/api/json/v1/1/list.php?i=list");    
        JSONObject obj = new JSONObject(fichero); 
        JSONArray drinks = obj.getJSONArray("drinks");
        int n = drinks.length();
        for (int i = 0; i < n; ++i) { 
            JSONObject cats = drinks.getJSONObject(i);
            String nombreIng = cats.getString("strIngredient1");
            listaIngredientes.add(new Ingrediente(nombreIng));
        } 
    }
    
    // Método que recibe el String fichero y va extrayendo la información de cada bebida del JSonArray para añadirlas al ObservableList de bebidas
    private void recorrerJson(String fichero) {
        JSONObject obj = new JSONObject(fichero);
        JSONArray drinks = obj.getJSONArray("drinks");
        int n = drinks.length();
        for (int i = 0; i < n; i++) {
            JSONObject obj2 = drinks.getJSONObject(i);
            int id = (int) obj2.getInt("idDrink");
            String nombre = (String) obj2.getString("strDrink");
            String imagen = (String) obj2.getString("strDrinkThumb");
            Bebida b = new Bebida(id, nombre, imagen);
            listaBebidas.add(b);
        }
    }
    
    // Cargar bebidas por categoría según la seleccionada en el choiceBox
    @FXML
    void mostrarCategorias(ActionEvent event) {
        if (cbCategorias.getSelectionModel().getSelectedIndex()>-1){
            listaBebidas.clear();
            try {
                String cat = cbCategorias.getSelectionModel().getSelectedItem().getNombre();
                String url = "https://www.thecocktaildb.com/api/json/v1/1/filter.php?c=" + cat;
                String fichero = extraerFichero(url);
                recorrerJson(fichero);
            añadirTableView(listaBebidas);    
            //listViewBebidas.setItems(listaBebidas);    
            } catch (IOException ex) {
                Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            Alert alert2 = new Alert(Alert.AlertType.ERROR);
            alert2.setTitle("Error");
            alert2.setContentText("No hay categoria seleccionada");
            alert2.showAndWait();
        }  
    }
    
    // Cargar bebidas por vaso según el tipo seleccionada en el choiceBox
    @FXML
    void mostrarVasos(ActionEvent event) {
        if (cbVasos.getSelectionModel().getSelectedIndex()>-1){
            listaBebidas.clear();
            try {
                String vas = cbVasos.getSelectionModel().getSelectedItem().getNombre();
                String url = "https://www.thecocktaildb.com/api/json/v1/1/filter.php?g=" + vas;
                String fichero = extraerFichero(url);
                recorrerJson(fichero);
                añadirTableView(listaBebidas);    
            //listViewBebidas.setItems(listaBebidas);    
            } catch (IOException ex) {
                Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            Alert alert2 = new Alert(Alert.AlertType.ERROR);
            alert2.setTitle("Error");
            alert2.setContentText("No hay tipo de vaso seleccionado");
            alert2.showAndWait();
        }  
    }

    // Cargar bebidas por ingredientes según el seleccionado en el choiceBox
    @FXML
    void mostrarIngredientes(ActionEvent event) {
        if (cbIngredientes.getSelectionModel().getSelectedIndex()>-1){
            listaBebidas.clear();
            try {
                String ing = cbIngredientes.getSelectionModel().getSelectedItem().getNombre();
                String url = "https://www.thecocktaildb.com/api/json/v1/1/filter.php?i=" + ing;
                String fichero = extraerFichero(url);
                recorrerJson(fichero);
                añadirTableView(listaBebidas);    
            //listViewBebidas.setItems(listaBebidas);    
            } catch (IOException ex) {
                Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            Alert alert2 = new Alert(Alert.AlertType.ERROR);
            alert2.setTitle("Error");
            alert2.setContentText("No hay ingrediente seleccionado");
            alert2.showAndWait();
        }  
    }
    
    // Cargar bebidas por filtro de Alcohol
        @FXML
    void mostrarAlcohol(ActionEvent event) throws IOException {
        if (btCon.isSelected()){
            listaBebidas.clear();
            String url = "https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=Alcoholic";
            String fichero = extraerFichero(url);
            recorrerJson(fichero);
            añadirTableView(listaBebidas);
        } else if (btSin.isSelected()) {
            listaBebidas.clear();
            String url = "https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=Non_Alcoholic";
            String fichero = extraerFichero(url);
            recorrerJson(fichero);
            añadirTableView(listaBebidas);
        } else {
            Alert alert2 = new Alert(Alert.AlertType.ERROR);
            alert2.setTitle("Error");
            alert2.setContentText("Selecciona una categoría");
            alert2.showAndWait();
        }    
    }
    
    // Carga una bebida aleatoria
    @FXML
    void bebidaAleatoria(ActionEvent event) throws IOException {
        String aleat = "https://www.thecocktaildb.com/api/json/v1/1/random.php";
        String fichero = extraerFichero(aleat);
        JSONObject obj = new JSONObject(fichero);
        JSONArray drinks = obj.getJSONArray("drinks");
        int n = drinks.length();
        for (int i = 0; i < n; i++) {
            JSONObject obj2 = drinks.getJSONObject(i);
            int id = (int) obj2.getInt("idDrink");
            String nombre = (String) obj2.getString("strDrink");
            String imagen = (String) obj2.getString("strDrinkThumb");
            Bebida b = new Bebida(id, nombre, imagen);
            
            Image bebida = new Image(b.getImagen());
            imgBebida.setImage(bebida);
            imgBebida.setImage(redondearImagen(imgBebida));
            
            lbInfo.setText(b.getNombre());
        }
    }
    
    // Buscar bebidas introduciendo el nombre o parte del nombre
    @FXML
    void buscarNombre(ActionEvent event) {
        if (!tfNombre.getText().isEmpty()) {
            try {
                listaBebidas.clear();
                String url = "https://www.thecocktaildb.com/api/json/v1/1/search.php?s=" + tfNombre.getText();
                String fichero;
                fichero = extraerFichero(url);
                recorrerJson(fichero);
                añadirTableView(listaBebidas);
            } catch (IOException ex) {
                Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            Alert alert2 = new Alert(Alert.AlertType.ERROR);
            alert2.setTitle("Error");
            alert2.setContentText("Introduce un nombre para buscar");
            alert2.showAndWait();
        }
    }
    
    private void añadirTableView(ObservableList<Bebida> listaBebidas) {
        this.colId.setCellValueFactory(new PropertyValueFactory("id"));
        this.colNombre.setCellValueFactory(new PropertyValueFactory("nombre"));
        this.tbBebidas.setItems(listaBebidas);
    }
    
    // Aplicar bordes redondeados a la imagen de las bebidas
    private Image redondearImagen(ImageView image) {
        Rectangle clip = new Rectangle();
        clip.setWidth(imgBebida.getFitWidth());
        clip.setHeight(imgBebida.getFitHeight());
        
        clip.setArcHeight(100);
        clip.setArcWidth(100);
        clip.setStroke(Color.BLACK);
        imgBebida.setClip(clip);
        
        SnapshotParameters parameters = new SnapshotParameters();
        parameters.setFill(Color.TRANSPARENT);
        WritableImage img = imgBebida.snapshot(parameters, null);
        
        imgBebida.setClip(null);
        
        imgBebida.setEffect(new DropShadow(40, Color.DARKVIOLET));
        return img;
    }
    
    // Cambiar imagen cuando haces click a una bebida de la tabla
    @FXML
    void btTabla(MouseEvent event) {
        if (tbBebidas.getSelectionModel().getSelectedItem()!=null) {
            Bebida bb = tbBebidas.getSelectionModel().getSelectedItem();
            Image bebida = new Image(bb.getImagen()); 
            imgBebida.setImage(bebida);   
        
            imgBebida.setImage(redondearImagen(imgBebida));
        
            lbInfo.setText(bb.getNombre());
        } 
    }
}
