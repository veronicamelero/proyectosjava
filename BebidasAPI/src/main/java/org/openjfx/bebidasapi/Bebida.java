
package org.openjfx.bebidasapi;

import java.util.ArrayList;

public class Bebida {
    public int id;
    public String nombre;
    public String imagen;

    public Bebida(int id, String nombre, String imagen) {
        this.nombre = nombre;
        this.imagen = imagen;
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    @Override
    public String toString() {
        return "Bebida{" + "id=" + id + ", nombre=" + nombre + ", imagen=" + imagen + '}';
    }

}
