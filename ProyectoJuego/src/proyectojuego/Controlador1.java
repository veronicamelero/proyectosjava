
package proyectojuego;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import static proyectojuego.Controlador3.silencio;



public class Controlador1 implements Initializable {
    
    public static Scene scene;
    public static Stage stageComienzo;
    private MediaPlayer musicaEntrada;
    
    @FXML
    private Button btIniciarJuego;
    
    @FXML
    private ImageView fondoInicio;
    @FXML
    private ImageView mute;
    @FXML
    private ImageView btSonido;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Image fondo = new Image(getClass().getResourceAsStream("/Imagenes/fondo1.jpg"));
        fondoInicio.setImage(fondo);
        sonido("src/Musica/DragonBallOpening.mp3");
    }
    
    @FXML
    void iniciarJuego(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("FXML2.fxml"));
            Parent root = loader.load();
            Controlador2 controlador2 = loader.getController();
            
            stageComienzo = (Stage) ((Node) event.getSource()).getScene().getWindow();
            scene = new Scene(root);
            stageComienzo.setScene(scene);
            stageComienzo.show();
            musicaEntrada.stop();
            
        } catch (IOException io) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Error");
            alert.setContentText(io.getMessage());
            alert.showAndWait();
        }
    }

    public void sonido(String url) {
            Media media = new Media(new File(url).toURI().toString());
            musicaEntrada = new MediaPlayer(media);
            musicaEntrada.setCycleCount(MediaPlayer.INDEFINITE);
            musicaEntrada = new MediaPlayer(media);
            musicaEntrada.play();
    }
    
    @FXML
    void quitarSonido(MouseEvent event) {
        musicaEntrada.setMute(true);
        silencio = true;
    }
    
    @FXML
    void ponerSonido(MouseEvent event) {
        silencio = false;
        musicaEntrada.setMute(false);
    }

}
