
package proyectojuego;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import static proyectojuego.Controlador3.silencio;


public class ControladorBolas implements Initializable {

    private MediaPlayer musicaDragon;
    
    @FXML
    private Button btDeseo;

    @FXML
    private ImageView gifConceder;
    
    @FXML
    private ImageView imgShenron;

    @FXML
    private Label label1;

    @FXML
    private Label label2;

    @FXML
    private Label label3;

    @FXML
    private TextField tfDeseo;
    
    @FXML
    void pedirDeseo(ActionEvent event) {
        label1.setVisible(false);
        label2.setVisible(false);
        label3.setVisible(false);
        tfDeseo.setVisible(false);
        btDeseo.setVisible(false);
        imgShenron.setVisible(false);
        gifConceder.setVisible(true);
        if (silencio == false){
            sonido("src/Musica/ChalaHeadChala.mp3");
        }

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        gifConceder.setVisible(false);

    }    
    
    public void sonido(String url) {
        Media media = new Media(new File(url).toURI().toString());
            musicaDragon = new MediaPlayer(media);
            musicaDragon.setCycleCount(MediaPlayer.INDEFINITE);
            musicaDragon.setVolume(0.6);
            musicaDragon = new MediaPlayer(media);
            musicaDragon.play();
    }
}
