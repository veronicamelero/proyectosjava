
package proyectojuego;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;


public class ProyectoJuego extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXML1.fxml"));
        
        Scene scene = new Scene(root);
        stage.getIcons().add(new Image(ProyectoJuego.class.getResourceAsStream("/Imagenes/icono2.png")));
        stage.setTitle("Dragon Ball Game");

        stage.setScene(scene);
        stage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
    
}
